import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtService } from '@nestjs/jwt';
import { Usuario } from 'src/entities';

describe('AuthService', () => {
  let service: AuthService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn().mockReturnValue('test-token'),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('Deberia de estar definido', () => {
    expect(service).toBeDefined();
  });
  it('Deberia de retornar el Token', async () => {
    const usuario: Usuario = { id: 1, email: 'usuario1@prueba.com' } as Usuario;
    const result = await service.login(usuario);
    expect(result).toEqual({ acces_token: 'test-token' });
    expect(jwtService.sign).toHaveBeenCalledWith({
      email: usuario.email,
      sub: usuario.id,
    });
  });
});
