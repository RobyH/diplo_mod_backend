import { VerificarDto } from 'src/usuario/dto/verificar.dto';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsuarioService } from 'src/usuario/usuario.service';
import { Test, TestingModule } from '@nestjs/testing';
import { Usuario } from 'src/entities';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;
  let usuarioService: UsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn().mockResolvedValue({ acces_token: 'test-token' }),
          },
        },
        {
          provide: UsuarioService,
          useValue: {
            verificarCuenta: jest.fn().mockResolvedValue({
              id: 1,
              email: 'test@example.com',
            } as Usuario),
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
    usuarioService = module.get<UsuarioService>(UsuarioService);
  });

  it('Deberia de estar definido', () => {
    expect(controller).toBeDefined();
  });

  it('Deberia de retornar el acceso con el token', async () => {
    const verificarDto: VerificarDto = {
      email: 'test@example.com',
      password: 'password',
    };
    const result = await controller.login(verificarDto);
    expect(result).toEqual({ acces_token: 'test-token' });
    expect(usuarioService.verificarCuenta).toHaveBeenCalledWith(verificarDto);
    expect(authService.login).toHaveBeenCalledWith({
      id: 1,
      email: 'test@example.com',
    });
  });
});
