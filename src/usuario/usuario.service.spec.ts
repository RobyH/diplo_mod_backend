import { Test, TestingModule } from '@nestjs/testing';
import { UsuarioService } from './usuario.service';
import { UsuarioController } from './usuario.controller';
// import {
//   CreateUsuarioDto,
//   UpdateUsuarioDto,
//   VerificarDto,
// } from './dto/index.dto';
import { Usuario } from './entities/usuario.entity';

describe('UsuarioController', () => {
  let controller: UsuarioController;
  let service: UsuarioService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsuarioController],
      providers: [
        {
          provide: UsuarioService,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
            update: jest.fn(),
            remove: jest.fn(),
            findByUserName: jest.fn(),
            findByEmail: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<UsuarioController>(UsuarioController);
    service = module.get<UsuarioService>(UsuarioService);
  });

  it('Deberia de estar definido', () => {
    expect(controller).toBeDefined();
  });

  describe('findByUserName', () => {
    it('Deberia de validar que no existe el nombre de Usuario', async () => {
      const nombreUsuario = 'usuario1';
      const usuarioMock: Usuario = {
        id: 1,
        nombreUsuario,
        email: 'usuario1@prueba.com',
        password: '123',
      };

      jest.spyOn(service, 'findByUserName').mockResolvedValue(usuarioMock);

      const result = await controller.findByUserName(nombreUsuario);

      expect(result).toEqual(usuarioMock);
    });
  });

  describe('findByEmail', () => {
    it('Deberia de validar el correo de un usuario', async () => {
      const email = 'usuario1@prueba.com';
      const usuarioMock: Usuario = {
        id: 1,
        nombreUsuario: 'usuario1',
        email,
        password: '123',
      };

      jest.spyOn(service, 'findByEmail').mockResolvedValue(usuarioMock);

      const result = await controller.findByEmail(email);

      expect(result).toEqual(usuarioMock);
    });
  });

  it('Deberia de crear a un usuario', async () => {
    const createUsuarioDto = {
      nombreUsuario: 'usuario2',
      password: 'password',
      email: 'usuario2@prueba.com',
    };

    const usuarioMock: Usuario = {
      id: 1,
      nombreUsuario: createUsuarioDto.nombreUsuario,
      email: createUsuarioDto.email,
      password: createUsuarioDto.password,
    };

    (service.create as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await controller.create(createUsuarioDto);

    expect(result).toEqual(usuarioMock);
  });

  it('Deberia de listar a los usuarios', async () => {
    const usuariosMock: Usuario[] = [
      {
        id: 1,
        nombreUsuario: 'user1',
        email: 'user1@prueba.com',
        password: '123',
      },
      {
        id: 2,
        nombreUsuario: 'user2',
        email: 'user2@prueba.com',
        password: '123',
      },
    ];

    (service.findAll as jest.Mock).mockResolvedValue(usuariosMock);

    const result = await controller.findAll();

    expect(result).toEqual(usuariosMock);
  });

  it('Deberia de devolver a un usuario', async () => {
    const id = '1';
    const usuarioMock: Usuario = {
      id: 1,
      nombreUsuario: 'usuario1',
      email: 'usuario1@prueba.com',
      password: '123',
    };

    (service.findOne as jest.Mock).mockResolvedValue(usuarioMock);

    const result = await controller.findOne(id);

    expect(result).toEqual(usuarioMock);
  });

  it('Deberia de actualizar a un usuario', async () => {
    const id = '1';
    const updateUsuarioDto = {
      nombreUsuario: 'usuario',
      email: 'usuario1Update@prueba.com',
    };
    const usuarioMock: Usuario = {
      id: 1,
      nombreUsuario: 'usuario1',
      email: 'usuario1@prueba.com',
      password: '123',
    };

    (service.update as jest.Mock).mockResolvedValue({
      ...usuarioMock,
      ...updateUsuarioDto,
    });

    const result = await controller.update(id, updateUsuarioDto);

    expect(result).toEqual({ ...usuarioMock, ...updateUsuarioDto });
  });

  it('Deberia de eliminar a un  usuario', async () => {
    const id = '1';
    // const usuarioMock: Usuario = {
    //   id: 1,
    //   nombreUsuario: 'usuario1',
    //   email: 'usuario1@prueba.com',
    //   password: '123',
    // };

    (service.remove as jest.Mock).mockResolvedValue({ affected: 1 });

    const result = await controller.remove(id);

    expect(result).toEqual({ affected: 1 });
  });
});
