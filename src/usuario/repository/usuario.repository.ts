import { Injectable } from '@nestjs/common';
import { CreateUsuarioDto, UpdateUsuarioDto } from '../dto/index.dto';
import { Usuario } from '../entities/usuario.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsuarioRepository {
  // constructor(private dataSource: DataSource) {} // Con constructor por tablas
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
  ) {}
  crear(createUsuarioDto: CreateUsuarioDto) {
    return this.usuarioRepository.save(createUsuarioDto);
  }
  actualizar(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    return this.usuarioRepository.update(id, updateUsuarioDto);
  }
  listar() {
    return this.usuarioRepository.find();
  }
  findById(id: number) {
    return this.usuarioRepository.findOneBy({ id });
  }
  eliminar(id: number) {
    return this.usuarioRepository.delete(id);
  }

  async findByUserName(nombreUsuario: string): Promise<Usuario> {
    return this.usuarioRepository.findOne({ where: { nombreUsuario } });
  }

  async findByEmail(email: string): Promise<Usuario> {
    return this.usuarioRepository.findOne({ where: { email } });
  }
}
