import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class VerificarDto {
  @IsEmail()
  @IsNotEmpty()
  @IsString()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(2, { message: 'Debe de tener al menos 2 caracteres' })
  password: string;
}
