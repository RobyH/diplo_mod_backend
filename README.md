# diplo_mod_backend



## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/RobyH/diplo_mod_backend.git
git branch -M main
git push -uf origin main
```

## Instalando dependencias

```bash
$ npm install
```

## Aplicar variables de entorno
```bash
$ cp .env.example .env
```

## Ejecutar la aplicación
```bash
$ npm run start:dev
```

## Base de datos
```bash
$ docker compose up -d
```

## Comando para realizar los test
``` bash
$ npm run test -- --verbose
```

## Comandos personalizados para los test por separado
``` bash
$ npm run test:usuario -- --verbose

$ npm run test:auth -- --verbose
